
function findCarWithId33(inventory)
{
    
    if(!Array.isArray(inventory))
    {
       return "The given data is not an array.";
       
    }

    let carInfo=inventory.find((item)=> item["id"]===33);
    
    return "Car 33 is a "+ carInfo.car_year+" "+ carInfo.car_make+" "+carInfo.car_model;
}

module.exports=findCarWithId33;