
function countOfCarsMadeBefore2000(inventory,listOfCarYears)
{
    if(!Array.isArray(inventory) || !Array.isArray(listOfCarYears))
    {
            return "The given data is not an array.";
    }   

    return listOfCarYears.filter((item)=> item<2000).length;
}

module.exports=countOfCarsMadeBefore2000;