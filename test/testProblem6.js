const problem6=require('../problem6');
const inventorySupplier=require('./inventorySupplier');

const inventory=inventorySupplier();
const result=problem6(inventory);

console.log(JSON.stringify(result));