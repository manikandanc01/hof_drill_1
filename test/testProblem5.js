const problem4=require('../problem4');
const problem5=require('../problem5');
const inventorySupplier=require('./inventorySupplier');


const inventory=inventorySupplier();
const listOfCarYears=problem4(inventory);

const result=problem5(inventory,listOfCarYears);

console.log(result);