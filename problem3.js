

function sortCarModelList(inventory)
{
    if(!Array.isArray(inventory))
    {
        return "The given data is not an array.";
    }

    let sizeOfTheInventory=inventory.length;
    
    //Create an array containg only car model names
    let carModelsList=[];

    inventory.forEach((item)=> carModelsList.push(item["car_model"]));

    carModelsList.sort();
   
    return carModelsList;  
}

module.exports=sortCarModelList;