function findTheLastCar(inventory)
{
    if(!Array.isArray(inventory))
    {
           return "The given data is not an array.";
    }

    //inventory.slice(-1); returns the last element as an Array [{}]
    let lastCar=inventory.slice(-1);

    return "Last car is a "+lastCar[0].car_make+" "+lastCar[0].car_model;

}

module.exports=findTheLastCar;