function listTheCarYears(inventory)
{
    if(!Array.isArray(inventory))
    {
        return "The given data is not an array.";
    }

    let sizeOfTheInventory=inventory.length;
    let listOfCarYears=[];

    //Create array contains only with years of the car
    inventory.forEach((item)=> listOfCarYears.push(item["car_year"]));

    return listOfCarYears;
}

module.exports=listTheCarYears;