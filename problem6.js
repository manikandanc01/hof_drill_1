function findBmwAndAudiCars(inventory)
{
    if(!Array.isArray(inventory))
    {
        return "The given data is not an array.";
    }


    let listOfBmwAndAudiCars=inventory.filter((item) => item["car_make"].toLowerCase()==="bmw" || item["car_make"].toLowerCase()==="audi");

    return listOfBmwAndAudiCars;

}

module.exports=findBmwAndAudiCars;